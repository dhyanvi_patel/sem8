﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpDesk.Models
{
    public class clsCommon
    {
        HelpDeskDataContext db = new HelpDeskDataContext();
        public int? getUserIdFromSession()
        {
            int? intid = 0;
            if (HttpContext.Current.Session["intUserId"] != null && HttpContext.Current.Session["intUserId"] != "")
            {
                intid = Convert.ToInt32(HttpContext.Current.Session["intUserId"]);
            }
            return intid;
        }
    }
}