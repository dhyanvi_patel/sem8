﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace HelpDesk.Models
{
    public class clsTicket
    {

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["HelpDeskConnectionString"].ConnectionString);
        HelpDeskDataContext db = new HelpDeskDataContext();
        clsCommon objCommon = new clsCommon();


        public int? intId { get; set; }
        public int? Type { get; set; }

        public int? intCategoryId { get; set; }
        public int? intPriorityId { get; set; }

        public string strTicketName { get; set; }
        public string strProblemDescription { get; set; }
        public string strCustomerName { get; set; }
        public string strCustomerEmail { get; set; }
        public int? strMobile { get; set; }
        public string strAddress { get; set; }
        public bool? bIsStatus { get; set; }

        public string strTechName { get; set; }

        public bool? bIsAssign { get; set; }
        public int intTotalEntries { get; set; }
        public int intShowingEntries { get; set; }
        public int intfromEntries { get; set; }
        public int? intPageSize { get; set; }
        public int? intPageIndex { get; set; }
        public int? intTotalRecord { get; set; }
        public int? intPageCount { get; set; }
        public int? ROWNUMBER { get; set; }
        public Pager Pager { get; set; }
        public string strCreatedDate { get; set; }
        public string strCreatedBy { get; set; }

        public List<clsTicket> LstTicketList { get; set; }
       

        public string strResponse { get; set; }



        public clsTicket addTicket(clsTicket cls) {
           
                try
                {
                   
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Sp_Ticket_Insert", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = cls.Type;
                    cmd.Parameters.Add("@intId", SqlDbType.Int).Value = cls.intId;
                    cmd.Parameters.Add("@strTicketName", SqlDbType.VarChar).Value = cls.strTicketName;
                    cmd.Parameters.Add("@intCategoryId", SqlDbType.Int).Value = cls.intCategoryId;
                    cmd.Parameters.Add("@strDescription", SqlDbType.VarChar).Value = cls.strProblemDescription;
                    cmd.Parameters.Add("@intPriorityId", SqlDbType.Int).Value = cls.intPriorityId;
                 // cmd.Parameters.Add("@bIsStatus", SqlDbType.Bit).Value = cls.bIsStatus;
                    cmd.Parameters.Add("@strCustomerName", SqlDbType.VarChar).Value = cls.strCustomerName;
                cmd.Parameters.Add("@strCustomerEmail", SqlDbType.VarChar).Value = cls.strCustomerEmail;
                    cmd.Parameters.Add("@intCustomerMobileNumber", SqlDbType.Int).Value = cls.strMobile;
                    cmd.Parameters.Add("@strCustomerAddress", SqlDbType.VarChar).Value = cls.strAddress;
                cmd.Parameters.Add("@strTechName", SqlDbType.VarChar).Value = cls.strTechName;
                cmd.Parameters.Add("@intLoginId", SqlDbType.Int).Value = objCommon.getUserIdFromSession();
                    cmd.Parameters.Add("@EntityId", SqlDbType.Int).Value = SqlDbType.Int;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    cmd.CommandTimeout = 0;
                    da.ReturnProviderSpecificTypes = true;
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    conn.Close();
                    //if (dt.Rows.Count > 0)
                    //{
                    //    string intRefId = dt.Rows[0][0].ToString();
                    //    if (intRefId == clsConstant.RECORD_EXISTS)
                    //    {
                    //        cls.strResponse = clsConstant.MESSAGE_EXISTS;
                    //    }
                    //    else
                    //    {
                    //        if (cls.intId == 0)
                    //        {
                    //            cls.strResponse = clsConstant.MESSAGE_SUCCESS;
                    //            cls.intId = Convert.ToInt32(intRefId);
                    //        }
                    //        else
                    //        {
                    //            cls.strResponse = clsConstant.MESSAGE_UPDATE;
                    //        }
                    //    }
                    //}
                }

            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return cls;
        }
    }
}