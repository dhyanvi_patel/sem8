﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpDesk.Models
{
    public class clsLogin
    {

        public int? intId { get; set; }
        public int? Type { get; set; }
        public string strName { get; set; }
        public string strEmail { get; set; }
        public string strPassword { get; set; }
        public string strResponse { get; set; }

        public clsLogin Login(clsLogin cls)
        {
            HelpDeskDataContext db = new HelpDeskDataContext();

            var data = (from item in db.tblUserMsts where item.strEmail == cls.strEmail && item.strPassword == cls.strPassword && item.intUserType == clsConstant.AdminUsertype select item).FirstOrDefault();
           var data1= (from item in db.tblUserMsts where item.strEmail == cls.strEmail && item.strPassword == cls.strPassword && item.intUserType == clsConstant.TechnicianUsertype select item).FirstOrDefault();
            if (data != null)
            {
                cls.strEmail = data.strEmail;
                cls.strName = data.strUserName;     
                cls.strPassword = data.strPassword;
                cls.intId = data.intId;
                cls.Type = data.intUserType;
                cls.strResponse = clsConstant.MESSAGE_SUCCESS;
            }
            else if (data1 != null)
            {
                cls.strEmail = data1.strEmail;
                cls.strName = data1.strUserName;
                cls.strPassword = data1.strPassword;
                cls.intId = data1.intId;
                cls.Type = data1.intUserType;
                cls.strResponse = clsConstant.MESSAGE_SUCCESS1;
            }
            return cls;
        }
    }
}
