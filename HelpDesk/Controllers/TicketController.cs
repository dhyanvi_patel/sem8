﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelpDesk.Models;
namespace HelpDesk.Controllers
{
    public class TicketController : Controller
    {

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["HelpDeskConnectionString"].ConnectionString);
        HelpDeskDataContext db = new HelpDeskDataContext();
        clsCommon objCommon = new clsCommon();
        // GET: Ticket
        public ActionResult Ticket()
        {
            return View();
        }

        public ActionResult ListTicket()
        {
            return View();
        }
        public ActionResult UpdateTicket()
        {
            return View();
        }

        

        [HttpPost, ValidateInput(false)]
        public ActionResult InsertTicket(clsTicket cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.InsertData;
                    cls = cls.addTicket(cls);
                    return Json(new { data = "success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public ActionResult GetSingleTicketData(clsTicket cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    tblTicketMaster tbl = new tblTicketMaster();
                    var getdata = (from r in db.tblTicketMasters where r.intId == cls.intId select r).FirstOrDefault();
                    return Json(data: getdata);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public ActionResult GetTicket(clsTicket cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["HelpDeskConnectionString"].ConnectionString);
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    int intTotalEntries = 0;
                    int intshowingEntries = 0;
                    int startentries = 0;
                    con.Open();
                   
                    List<clsTicket> lstTicketlist = new List<clsTicket>();
                    SqlCommand cmd = new SqlCommand("Sp_Get_Ticket", con);
                    cmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    da.Fill(dt);
                    con.Close();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            clsTicket obj = new clsTicket();
                            obj.intId = Convert.ToInt32(dt.Rows[i]["intId"] == null || dt.Rows[i]["intId"].ToString().Trim() == "" ? null : dt.Rows[i]["intId"].ToString());
                            obj.strTicketName = dt.Rows[i]["strTicketName"] == null || dt.Rows[i]["strTicketName"].ToString().Trim() == "" ? null : dt.Rows[i]["strTicketName"].ToString();
                            obj.intCategoryId = Convert.ToInt32(dt.Rows[i]["intCategoryId"] == null || dt.Rows[i]["intCategoryId"].ToString().Trim() == "" ? null : dt.Rows[i]["intCategoryId"].ToString());
                            obj.intPriorityId = Convert.ToInt32(dt.Rows[i]["intPriorityId"] == null || dt.Rows[i]["intPriorityId"].ToString().Trim() == "" ? null : dt.Rows[i]["intPriorityId"].ToString());
                            obj.strProblemDescription = dt.Rows[i]["strDescription"] == null || dt.Rows[i]["strDescription"].ToString().Trim() == "" ? null : dt.Rows[i]["strDescription"].ToString();
                            obj.strCustomerName = dt.Rows[i]["strCustomerName"] == null || dt.Rows[i]["strCustomerName"].ToString().Trim() == "" ? null : dt.Rows[i]["strCustomerName"].ToString();
                           // obj.strMobile = Convert.ToInt32(dt.Rows[i]["intCustomerMobileNumber"] == null || dt.Rows[i]["intCustomerMobileNumber"].ToString().Trim() == "" ? null : dt.Rows[i]["intCustomerMobileNumber"].ToString());
                            obj.strCustomerEmail = dt.Rows[i]["strCustomerEmail"] == null || dt.Rows[i]["strCustomerEmail"].ToString().Trim() == "" ? null : dt.Rows[i]["strCustomerEmail"].ToString();
                            obj.strAddress = dt.Rows[i]["strCustomerAddress"] == null || dt.Rows[i]["strCustomerAddress"].ToString().Trim() == "" ? null : dt.Rows[i]["strCustomerAddress"].ToString();
                            //obj.intCreatedby = Convert.ToInt32(dt.Rows[i]["intCreatedBy"] == null || dt.Rows[i]["intCreatedBy"].ToString().Trim() == "" ? null : dt.Rows[i]["intCreatedBy"].ToString());
                            obj.bIsStatus = Convert.ToBoolean(dt.Rows[i]["bIsStatus"] == null || dt.Rows[i]["bIsStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["bIsStatus"].ToString());
                            obj.strCreatedDate = dt.Rows[i]["strCreatedDate"] == null || dt.Rows[i]["strCreatedDate"].ToString().Trim() == "" ? null : Convert.ToDateTime(dt.Rows[i]["strCreatedDate"]).ToString("dd/MM/yyyy");
                            obj.intPageCount = Convert.ToInt32(dt.Rows[i]["intPageCount"] == null || dt.Rows[i]["intPageCount"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageCount"].ToString());
                            obj.intPageSize = Convert.ToInt32(dt.Rows[i]["intPageSize"] == null || dt.Rows[i]["intPageSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageSize"].ToString());
                            obj.intPageIndex = Convert.ToInt32(dt.Rows[i]["intPageIndex"] == null || dt.Rows[i]["intPageIndex"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageIndex"].ToString());
                            obj.intTotalRecord = Convert.ToInt32(dt.Rows[i]["intTotalRecord"] == null || dt.Rows[i]["intTotalRecord"].ToString().Trim() == "" ? null : dt.Rows[i]["intTotalRecord"].ToString());
                            obj.ROWNUMBER = Convert.ToInt32(dt.Rows[i]["ROWNUMBER"] == null || dt.Rows[i]["ROWNUMBER"].ToString().Trim() == "" ? null : dt.Rows[i]["ROWNUMBER"].ToString());
                            //obj.bIsAssign = Convert.ToBoolean(dt.Rows[i]["bIsAssign"] == null || dt.Rows[i]["bIsAssign"].ToString().Trim() == "" ? null : dt.Rows[i]["bIsAssign"].ToString());
                            //obj.strTechName = dt.Rows[i]["strTechName"] == null || dt.Rows[i]["strTechName"].ToString().Trim() == "" ? null : dt.Rows[i]["strTechName"].ToString();
                            lstTicketlist.Add(obj);

                        }
                    }
                    cls.LstTicketList = lstTicketlist;
                    if (cls.LstTicketList.Count > 0)
                    {
                        var pager = new Pager((int)cls.LstTicketList[0].intTotalRecord, cls.intPageIndex);

                        cls.Pager = pager;
                    }
                    cls.intTotalEntries = intTotalEntries;
                    cls.intShowingEntries = intshowingEntries;
                    cls.intfromEntries = startentries;

                    return PartialView("_TicketListPartial", cls);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult deleteTicket(clsTicket cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.DeleteData;
                    cls = cls.addTicket(cls);
                    return Json(cls, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}