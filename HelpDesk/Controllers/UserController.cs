﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelpDesk.Models;

namespace HelpDesk.Controllers
{
    public class UserController : Controller
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["HelpDeskConnectionString"].ConnectionString);
        HelpDeskDataContext db = new HelpDeskDataContext();
        clsCommon objCommon = new clsCommon();

        // GET: User
        public ActionResult User()
        {
            return View();
        }

        public ActionResult ListUser()
        {
            return View();
        }
        public ActionResult UpdateUser()
        {
            return View();
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult InsertUser(clsUser cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.InsertData;
                    cls = cls.addUser(cls);
                    return Json(new { data = "success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult GetSingleUserData(clsUser cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    tblUserMst tbl = new tblUserMst();
                    var getdata = (from r in db.tblUserMsts where r.intId == cls.intId select r).FirstOrDefault();
                    return Json(data: getdata);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public ActionResult GetUser(clsUser cls)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["HelpDeskConnectionString"].ConnectionString);
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    int intTotalEntries = 0;
                    int intshowingEntries = 0;
                    int startentries = 0;
                    con.Open();

                    List<clsUser> lstUserlist = new List<clsUser>();
                    SqlCommand cmd = new SqlCommand("Sp_Get_User", con);
                    cmd.CommandTimeout = 0;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    da.Fill(dt);
                    con.Close();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                        {
                            clsUser obj = new clsUser();
                            obj.intId = Convert.ToInt32(dt.Rows[i]["intId"] == null || dt.Rows[i]["intId"].ToString().Trim() == "" ? null : dt.Rows[i]["intId"].ToString());
                            obj.strFullName = dt.Rows[i]["strFullName"] == null || dt.Rows[i]["strFullName"].ToString().Trim() == "" ? null : dt.Rows[i]["strFullName"].ToString();
                            obj.strUserName = dt.Rows[i]["strUserName"] == null || dt.Rows[i]["strUserName"].ToString().Trim() == "" ? null : dt.Rows[i]["strUserName"].ToString();
                            obj.intUserType = Convert.ToInt32(dt.Rows[i]["intUserType"] == null || dt.Rows[i]["intUserType"].ToString().Trim() == "" ? null : dt.Rows[i]["intUserType"].ToString());
                            
                            // obj.strMobile = Convert.ToInt32(dt.Rows[i]["intCustomerMobileNumber"] == null || dt.Rows[i]["intCustomerMobileNumber"].ToString().Trim() == "" ? null : dt.Rows[i]["intCustomerMobileNumber"].ToString());
                            obj.strEmail = dt.Rows[i]["strEmail"] == null || dt.Rows[i]["strEmail"].ToString().Trim() == "" ? null : dt.Rows[i]["strEmail"].ToString();
                            //obj.strAddress = dt.Rows[i]["strCustomerAddress"] == null || dt.Rows[i]["strCustomerAddress"].ToString().Trim() == "" ? null : dt.Rows[i]["strCustomerAddress"].ToString();
                            //obj.intCreatedby = Convert.ToInt32(dt.Rows[i]["intCreatedBy"] == null || dt.Rows[i]["intCreatedBy"].ToString().Trim() == "" ? null : dt.Rows[i]["intCreatedBy"].ToString());
                          //  obj.bIsStatus = Convert.ToBoolean(dt.Rows[i]["bIsStatus"] == null || dt.Rows[i]["bIsStatus"].ToString().Trim() == "" ? null : dt.Rows[i]["bIsStatus"].ToString());
                            obj.strCreatedDate = dt.Rows[i]["strCreatedDate"] == null || dt.Rows[i]["strCreatedDate"].ToString().Trim() == "" ? null : Convert.ToDateTime(dt.Rows[i]["strCreatedDate"]).ToString("dd/MM/yyyy");
                            obj.intPageCount = Convert.ToInt32(dt.Rows[i]["intPageCount"] == null || dt.Rows[i]["intPageCount"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageCount"].ToString());
                            obj.intPageSize = Convert.ToInt32(dt.Rows[i]["intPageSize"] == null || dt.Rows[i]["intPageSize"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageSize"].ToString());
                            obj.intPageIndex = Convert.ToInt32(dt.Rows[i]["intPageIndex"] == null || dt.Rows[i]["intPageIndex"].ToString().Trim() == "" ? null : dt.Rows[i]["intPageIndex"].ToString());
                            obj.intTotalRecord = Convert.ToInt32(dt.Rows[i]["intTotalRecord"] == null || dt.Rows[i]["intTotalRecord"].ToString().Trim() == "" ? null : dt.Rows[i]["intTotalRecord"].ToString());
                            obj.ROWNUMBER = Convert.ToInt32(dt.Rows[i]["ROWNUMBER"] == null || dt.Rows[i]["ROWNUMBER"].ToString().Trim() == "" ? null : dt.Rows[i]["ROWNUMBER"].ToString());
                            
                            lstUserlist.Add(obj);

                        }
                    }
                    cls.LstUserList = lstUserlist;
                    if (cls.LstUserList.Count > 0)
                    {
                        var pager = new Pager((int)cls.LstUserList[0].intTotalRecord, cls.intPageIndex);

                        cls.Pager = pager;
                    }
                    cls.intTotalEntries = intTotalEntries;
                    cls.intShowingEntries = intshowingEntries;
                    cls.intfromEntries = startentries;

                    return PartialView("_UserListPartial", cls);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult deleteUser(clsUser cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.DeleteData;
                    cls = cls.addUser(cls);
                    return Json(cls, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}