﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data.SqlClient;
using HelpDesk.Models;

namespace HelpDesk.Controllers
{
    public class AssignController : Controller
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["HelpDeskConnectionString"].ConnectionString);
        HelpDeskDataContext db = new HelpDeskDataContext();
        clsCommon objCommon = new clsCommon();
        // GET: Assign
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AssignTicket()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult InsertTicket(clsTicket cls)
        {
            try
            {
                if (objCommon.getUserIdFromSession() != 0)
                {
                    cls.Type = clsConstant.InsertData;
                    cls = cls.addTicket(cls);
                    return Json(new { data = "success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}