﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelpDesk.Models;

namespace HelpDesk.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(clsLogin cls)
        {
            try
            {

                cls = cls.Login(cls);
                if (cls.intId > 0)
                {
                    Session["intUserId"] = cls.intId;
                    Session["UserName"] = cls.strName;
                    Session["Email"] = cls.strEmail;
                    Session["strPassword"] = cls.strPassword;
                    Session["Type"] = cls.Type;
                    //Session.Timeout = 2500;
                    //var data = ConvertViewToString("_MenuRender", GetMenuHTML()).ToString();
                    //Session["strMenuHTML"] = data;
                    return Json(cls, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    cls.strResponse = clsConstant.MESSAGE_FAIL;
                }
                return Json(cls, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in Login" + ex.Message);
            }
        }



    }
}